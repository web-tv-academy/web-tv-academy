from web_tv_academy import db
from web_tv_academy.models.user import User
import uuid


def __init__():
    """ Generate database."""
    db.drop_all()  # Supprime la base de données existante
    db.create_all()  # créé une base de donnée vierge
    new_user = User(
        id=uuid.uuid4(),
        first_name="first_name",
        last_name="last_name",
        username="usernme",
        email="mon@email.fr",
        password_hash="1234",
        phone_number="0102030405",
    )

    db.session.add(new_user)  # Insert le nouvel utilisateur dans la base de données
    db.session.commit()  # Sauvegarde la bdd
