from setuptools import setup

setup(
    name="web_tv_academy",
    version="0.1.0",
    description="Web Tv Academy webapp",
    author="Web Tv Academy team",
    packages=["web_tv_academy"],
    install_requires=[
        "flask",
        "flask-sqlalchemy",
        "safrs",
        "slugify",
        "Flask-WTF",
        "flask_uuid",
        "ffmpeg",
    ],
    python_requires=">3.8, <4",  # To be able to use the walrus operator
    url="https://gitlab.com/web-tv-academy/web-tv-academy",
    license="MIT",
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 3 - Alpha",
        # Indicate who your project is intended for
        "Intended Audience :: Developers",
        # Pick your license as you wish (should match "license" above)
        "License :: OSI Approved :: MIT License",
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    project_urls={
        "Documentation": "https://gitlab.com/web-tv-academy/web-tv-academy",
        "Source": "https://gitlab.com/web-tv-academy/web-tv-academy",
        "Tracker": "https://gitlab.com/web-tv-academy/web-tv-academy/-/issues",
    },
)
