# Installation

## Development mode

1. Create a virtual environment : `python -m venv env`
2. Enter the environment : `source ./venv/bin/activate`
3. Upgrade pip : `pip install -U pip`
4. Install tox : `pip intall tox`
5. Install the project : `python setup.py develop`
6. Run the project : `flask run`

## Daily development

* Enter the virtual environment : `source ./venv/bin/activate`
* Lint the code : `tox`
* Reformat the code (if needed) : `tox -e format`
* Run the development server : `flask run`

## Create Database

* Enter the virtual environment : `source ./venv/bin/activate`
* Create BDD & new user: `python3 database.py`
