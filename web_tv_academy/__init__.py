"""Web tv academy."""

from flask import Flask, render_template

from .config import config
from .models.api import create_api
from .models.database import db

app = Flask(__name__)
app.config.from_object(config["development"])
config["development"].init_app(app)

with app.app_context():
    db.init_app(app)
    api = create_api(app, prefix="/api/v1")

from .media import media as media_blueprint

app.register_blueprint(media_blueprint)
