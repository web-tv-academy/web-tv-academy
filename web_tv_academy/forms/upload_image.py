"""Upload media image form."""

from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField, FileRequired
from wtforms import (
    BooleanField,
    SelectMultipleField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.validators import DataRequired, Length, Optional


class UploadImage(FlaskForm):
    """Form upload Image media."""

    title = StringField(
        "Titre de la réalisation",
        validators=[DataRequired("Le titre de la réalisaiton est requis.")],
        render_kw={"class": "form-control", "placeholder": "Titre de la réalisation"},
    )
    description = TextAreaField(
        "Description de la réalisation",
        validators=[DataRequired("La description de la réalisaiton est requise.")],
        render_kw={
            "class": "form-control",
            "placeholder": "Description de la réalisation",
        },
    )
    media = FileField(
        "Upload de l'image",
        validators=[
            FileRequired("Le média est requis."),
            FileAllowed(["jpeg", "jpg", "png", "svg"], "Images only!"),
        ],
        render_kw={"class": "form-control", "placeholder": "Upload de l'image"},
    )
    private = BooleanField(
        "La réalisation est privée",
        validators=[Optional()],
        render_kw={
            "class": "form-check-input",
            "placeholder": "la réalisation est privée",
        },
    )
    tags = SelectMultipleField(
        "Tags liés à la réalisation",
        coerce=str,
        choices=[],
        render_kw={
            "class": "form-control d-none list-tags",
            "id": "list-tags",
            "placeholder": "Tags liés à la réalisation",
            "autocomplete": "off",
        },
    )
    submit = SubmitField(
        "Ajouter la réalisation", None, render_kw={"class": "btn btn-primary mt-5"}
    )
