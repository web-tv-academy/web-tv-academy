import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID

from ..helpers import guid
from .database import db

tags = db.Table(
    "has_tag",
    db.Column("tag_id", UUID(as_uuid=True), db.ForeignKey("tags.id"), primary_key=True),
    db.Column(
        "media_id", UUID(as_uuid=True), db.ForeignKey("medias.id"), primary_key=True
    ),
)


class Tag(db.Model):
    """description: Tag model."""

    __tablename__ = "tags"

    id = db.Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True
    )
    label = db.Column(db.String(50), nullable=False)
