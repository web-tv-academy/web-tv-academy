import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID

from ..helpers import guid
from .database import db


class Resolution(db.Model):
    """description: Resolution model."""

    __tablename__ = "resolutions"

    id = db.Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True
    )
    label = db.Column(db.String, nullable=False)
    type = db.Column(db.String, nullable=False)
    url = db.Column(db.Text, nullable=False)
    media_id = db.Column(UUID(as_uuid=True), db.ForeignKey("medias.id"))

    def __repr__(self):
        """Represent Resolution Class."""
        return "<Resolution %r>" % self.label
