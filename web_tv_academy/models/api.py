from flask import Blueprint
from safrs import SAFRSAPI

from .media import Media
from .user import User


def create_api(app, **api_args):
    """Create and expose API."""
    api = SAFRSAPI(app, **api_args)
    # api.expose_object(User)
    # api.expose_object(Media)
    return app
