"""database.

module containing the base database object to be linked to the flask app
using db.init_app(app)
"""


from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
