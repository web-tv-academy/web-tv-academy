import uuid

from flask_sqlalchemy import SQLAlchemy
from safrs import SAFRSBase
from sqlalchemy.dialects.postgresql import UUID

from .database import db
from .resolution import Resolution
from .tags import tags


class Media(db.Model):
    """description: Media model."""

    __tablename__ = "medias"

    # id = db.Column(UUID(as_uuid=True), primary_key=True)
    id = db.Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True
    )
    slug = db.Column(db.String, nullable=False)
    title = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=False)
    date_publication = db.Column(db.DateTime, nullable=False)
    is_private = db.Column(db.Boolean, nullable=False, default=False)
    media_type = db.Column(db.Enum("video", "image", name="media_type"), nullable=False)
    tags = db.relationship(
        "Tag", secondary=tags, lazy="subquery", backref=db.backref("medias", lazy=True)
    )

    resolutions = db.relationship("Resolution", lazy="dynamic")

    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey("users.id"))
    user = db.relationship("User", foreign_keys=[user_id])

    views = db.relationship("View")

    def __repr__(self):
        """Represent Media Class."""
        return "<Media %r>" % self.title
