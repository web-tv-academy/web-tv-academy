import uuid

from flask_sqlalchemy import SQLAlchemy
from safrs import SAFRSBase
from sqlalchemy.dialects.postgresql import UUID

from .database import db


class User(db.Model):
    """description: User model."""

    __tablename__ = "users"

    id = db.Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True
    )
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    username = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    password_hash = db.Column(db.String, nullable=False)
    phone_number = db.Column(db.String)

    def __repr__(self):
        """Represent user Class."""
        return "<User %r>" % self.username
