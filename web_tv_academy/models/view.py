import uuid

from flask_sqlalchemy import SQLAlchemy
from safrs import SAFRSBase
from sqlalchemy.dialects.postgresql import UUID

from .database import db


class View(db.Model):
    """description: view model for a media."""

    __tablename__ = "views"
    id = db.Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True
    )
    media_id = db.Column(UUID(as_uuid=True), db.ForeignKey("medias.id"))
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey("users.id"))
    date_view = db.Column(db.DateTime, nullable=True)
