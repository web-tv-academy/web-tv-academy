import mimetypes
import os

from flask import (
    Flask,
    flash,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)
from sqlalchemy import desc

from web_tv_academy import app

from ..forms.upload_video import UploadVideo
from ..models.database import db
from ..models.media import Media
from ..models.resolution import Resolution
from ..models.tags import Tag
from ..models.user import User
from ..models.view import View
from .function import add_view
from .upload import allowed_file, create_media, save_in_database, upload_quality


@app.route("/")
def home():
    """Show home page."""
    last_videos = (
        Media.query.filter_by(is_private=False, media_type="video")
        .order_by(desc("date_publication"))
        .limit(3)
        .all()
    )
    return render_template("home.html", videos=last_videos)


@app.route("/list/v")
def list_video():
    """List of public videos."""
    list_tags = Tag.query.all()
    videos = Media.query.filter_by(is_private=False, media_type="video").all()
    return render_template("video/list.html", videos=videos, tags=list_tags)


@app.route("/watch/v/<uuid>")
def watch_video(uuid):
    """watch_video."""
    video = Media.query.get_or_404(uuid)
    add_view(video.id)

    return render_template(
        "video/watch.html", data=video, resolutions=video.resolutions[0]
    )


@app.route("/ressource/v/<uuid>/")
@app.route("/ressource/v/<uuid>/<definition>")
def get_video_chunk(uuid, definition="video.m3u8"):
    """Return video chunk if it founded."""
    filename = Resolution.query.filter_by(media_id=uuid).first_or_404().url

    file = os.path.dirname(filename) + "/" + definition
    return send_from_directory(app.config["UPLOAD_FOLDER"], file)


@app.route("/upload/v", methods=["GET", "POST"])
def upload_video():
    """upload_video."""
    media = "null"
    form = UploadVideo()
    tags = Tag.query.all()
    form.tags.choices = [(str(i.id), i.label) for i in tags]
    if form.validate_on_submit():
        file = form.media.data
        if file and allowed_file(file.filename, app.config["ALLOWED_VIDEO_EXTENSIONS"]):
            media = create_media(form, "video")

            flash(
                'Votre vidéo a été enregistrée. <a href="'
                + url_for("watch_video", uuid=media.id)
                + '">Voir votre vidéo</a>',
                "success",
            )
        else:
            flash(
                "Le format de votre vidéo n'est pas supportée.",
                "danger",
            )

    return render_template(
        "video/upload.html", media=media, method=request.method, form=form, tags=tags
    )
