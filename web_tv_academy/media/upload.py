import datetime
import mimetypes
import os
import socket
import subprocess  # nosec
import sys
import uuid

import ffmpeg_streaming
from ffmpeg_streaming import Formats
from flask import render_template, send_from_directory
from flask_uuid import FlaskUUID
from slugify import slugify
from sqlalchemy import desc
from werkzeug.utils import secure_filename

from web_tv_academy import app

from ..models.database import db
from ..models.media import Media
from ..models.resolution import Resolution
from ..models.tags import Tag
from ..models.user import User

FlaskUUID(app)

# import ffmpeg_streaming
# from ffmpeg_streaming import Bitrate, Formats, Representation, Size


def allowed_file(filename, config) -> bool:
    """Return allowed files extensions of not."""
    return mimetypes.guess_type(filename)[0] in config


def upload_quality(name, label, url, file, media):
    """Save file path to BDD."""
    new_resolution = Resolution(label=name, type=label, url=url, media_id=media.id)
    db.session.add(new_resolution)
    db.session.commit()


def generate_thumbnail(media):
    """Generate thumbnail.jpg for the media."""
    path = os.path.dirname(media) + "/thumbnail.jpg"
    args = [
        "ffmpeg",
        "-i",
        media,
        "-vf",
        "thumbnail,scale=300:200",
        "-frames:v",
        "1",
        path,
    ]
    subprocess.check_output(args)  # nosec


def transcode_video(video):
    """Transcode video in 240p, 300p, 480p, 720p."""
    transcoding_video = ffmpeg_streaming.input(video)
    hls = transcoding_video.hls(Formats.h264())
    hls.auto_generate_representations()
    hls.output(
        os.path.dirname(video) + "/video.m3u8",
        monitor=monitor,
    )


def save_in_database(form, type):
    """Save media in BDD."""
    new_media = Media(
        slug=slugify(form.title.data),
        title=form.title.data,
        description=form.description.data,
        date_publication=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        is_private=form.private.data,
        user=User.query.get_or_404("f5a99fd4-eaea-4a12-b23b-bbd2051988a3"),
        media_type=type,
    )

    for tag in form.tags.data:
        new_tag = Tag.query.filter_by(id=tag).first()
        new_media.tags.append(new_tag)

    db.session.add(new_media)
    db.session.commit()
    return new_media


def create_media(form, type):
    """Create media."""
    file = form.media.data
    file_name = secure_filename(str(uuid.uuid4()))
    try:
        os.mkdir(os.path.join(app.config["UPLOAD_FOLDER"], file_name))
    except OSError:
        print("Creation of the directory %s failed" % path)
    else:
        if type == "video":
            file_name = file_name + "/video" + os.path.splitext(file.filename)[1]
            file.save(
                os.path.join(
                    app.config["UPLOAD_FOLDER"],
                    file_name,
                )
            )
        else:
            file_name = file_name + "/image" + os.path.splitext(file.filename)[1]
            file.save(
                os.path.join(
                    app.config["UPLOAD_FOLDER"],
                    file_name,
                )
            )

        media = save_in_database(form, type)
        upload_quality("Upload Quality", "UQ", file_name, file, media)
        generate_thumbnail(os.path.join(app.config["UPLOAD_FOLDER"], file_name))

        if type == "video":
            transcode_video(os.path.join(app.config["UPLOAD_FOLDER"], file_name))
    return media


@app.route("/upload")
def upload_index():
    """Show upload option."""
    return render_template("upload_index.html")


def monitor(ffmpeg, duration, time_, time_left, process):
    """Handle proccess.

    Examples:
    1. Logging or printing ffmpeg command
    logging.info(ffmpeg) or print(ffmpeg)

    2. Handling Process object
    if "something happened":
        process.terminate()

    3. Email someone to inform about the time of finishing process
    if time_left > 3600 and not already_send:  # if it takes more than one hour and you have not emailed them already
        ready_time = time_left + time.time()
        Email.send(
            email='someone@somedomain.com',
            subject='Your video will be ready by %s' % datetime.timedelta(seconds=ready_time),
            message='Your video takes more than %s hour(s) ...' % round(time_left / 3600)
        )
       already_send = True

    4. Create a socket connection and show a progress bar(or other parameters) to your users
    Socket.broadcast(
        address=127.0.0.1
        port=5050
        data={
            percentage = per,
            time_left = datetime.timedelta(seconds=int(time_left))
        }
    )

    :param ffmpeg: ffmpeg command line
    :param duration: duration of the video
    :param time_: current time of transcoded video
    :param time_left: seconds left to finish the video process
    :param process: subprocess object
    :return: None
    """
    per = round(time_ / duration * 100)

    sys.stdout.write(
        "\rTranscoding...(%s%%) %s left [%s%s]"
        % (
            per,
            datetime.timedelta(seconds=int(time_left)),
            "#" * per,
            "-" * (100 - per),
        )
    )
    sys.stdout.flush()
