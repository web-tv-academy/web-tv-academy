import os

from flask import (
    Flask,
    flash,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)

from web_tv_academy import app

from ..forms.upload_image import UploadImage
from ..models.database import db
from ..models.media import Media
from ..models.resolution import Resolution
from ..models.tags import Tag
from .function import add_view
from .upload import allowed_file, create_media, save_in_database, upload_quality


@app.route("/watch/i/<uuid>")
def watch_image(uuid):
    """watch_image."""
    image = Media.query.get_or_404(uuid)
    add_view(image.id)
    return render_template(
        "image/watch.html", data=image, resolutions=image.resolutions[0]
    )


@app.route("/list/i")
def list_image():
    """List of public videos."""
    list_tags = Tag.query.all()
    images = Media.query.filter_by(is_private=False, media_type="image").all()
    return render_template("image/list.html", images=images, tags=list_tags)


@app.route("/ressource/i/<uuid>")
def get_image_chunk(uuid):
    """Return image chunk if it founded."""
    filename = Resolution.query.filter_by(media_id=uuid).first_or_404().url

    return send_from_directory(app.config["UPLOAD_FOLDER"], filename)


@app.route("/thumbnail/<uuid>")
def get_thumbnail_file(uuid):
    """Return the thumbnail of the video if it founded."""
    media = Media.query.get_or_404(uuid)
    filename = media.resolutions.first_or_404().url
    return send_from_directory(
        app.config["UPLOAD_FOLDER"], os.path.dirname(filename) + "/thumbnail.jpg"
    )


@app.route("/upload/i", methods=["GET", "POST"])
def upload_image():
    """upload_image."""
    media = "null"
    form = UploadImage()
    tags = Tag.query.all()
    form.tags.choices = [(str(i.id), i.label) for i in tags]
    if form.validate_on_submit():
        file = form.media.data
        if file and allowed_file(file.filename, app.config["ALLOWED_IMAGE_EXTENSIONS"]):
            media = create_media(form, "image")

            flash(
                'Votre image a été enregistrée. <a href="'
                + url_for("watch_image", uuid=media.id)
                + '">Voir votre réalisation</a>',
                "success",
            )
        else:
            flash(
                "Le format de votre image n'est pas supportée.",
                "danger",
            )

    return render_template(
        "image/upload.html", media=media, method=request.method, form=form, tags=tags
    )
