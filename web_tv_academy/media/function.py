import datetime

from ..models.database import db

# from ..models.view import View
from ..models.user import User
from ..models.view import View


def add_view(media_id):
    """Add a view to video_id media."""
    user = User.query.get_or_404("f5a99fd4-eaea-4a12-b23b-bbd2051988a3")

    view = View(
        media_id=media_id,
        user_id=user.id,
        date_view=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    )

    db.session.add(view)
    db.session.commit()
