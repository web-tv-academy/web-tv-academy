"""Media package."""
from flask import (
    Blueprint,
    Flask,
    flash,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)

from web_tv_academy import app

media = Blueprint("media", __name__)
from . import image, video
